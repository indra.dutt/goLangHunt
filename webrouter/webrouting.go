package webrouter

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"sync"
	"errors"
)

type GinRoutingController interface {
	GetRouter() *gin.Engine
	RunInsecureRouter(portNumber string) error
	RunSecureRouter(portNumber string, certFileName string, keyFileName string) error
}

const (
	STATUS_REPORT_ROUTE string = "/status"
)

var singletonManager sync.Once

var routing WebRoutingServices

type WebRoutingServices struct {
	webrouter *gin.Engine
}

func (rm *WebRoutingServices) GetRouter() *gin.Engine {
	return rm.webrouter
}

func (rm WebRoutingServices) RunInsecureRouter(portNumber string) error {
	return rm.webrouter.Run(":" + portNumber)
}

func (rm WebRoutingServices) RunSecureRouter(portNumber string, certFileName string, keyFileName string) error {
	return errors.New("Not implemented yet")
}

func InitWebRoutingServices() GinRoutingController {
	singletonManager.Do(func() {
		// create the router
		routing = WebRoutingServices{gin.Default()}

		// add default routes
		r := routing.GetRouter()
		r.GET(STATUS_REPORT_ROUTE, func(c *gin.Context) {
			c.JSON(http.StatusOK, gin.H{"uptime": "1"})
		})
	})
	return &routing
}
