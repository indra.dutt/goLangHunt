package main

import (
	"net/http"
	"testing"
	"io/ioutil"
	"strings"
	"fmt"
	"math/rand"
)

func TestGet(t *testing.T) {
	url := "http://localhost:8192/"
	req, err := http.NewRequest("GET", url, nil)
	if err != nil{
		logger.I("request gen failed")
		return
	}
	client := http.DefaultClient
	response, err := client.Do(req)
	if err != nil {
		logger.I("error while execution")
		return
	}
	defer response.Body.Close()
	body, _ := ioutil.ReadAll(response.Body)

	logger.I(response.StatusCode)
	logger.I(string(body))
}

func TestAddEater(t *testing.T)  {
	url := "http://localhost:8192/eaters"
	jsonDef := `{
		"password": "password1",
		"email": "%s",
		"firstName" : "Food",
		"lastName" : "Fonder"
	}`
	email := fmt.Sprintf("user%d@user.com", rand.Int31())
	jsonBody := fmt.Sprintf(jsonDef, email)

	req, err := http.NewRequest("POST", url, strings.NewReader(jsonBody) )
	if err != nil {
		logger.D(err)
		t.Fail()
	}
	req.Header.Set("Content-Type", "application/json")
	client := &http.Client{}
	response, err := client.Do(req)
	if err != nil {
		logger.D(err)
		t.Fail()
	}
	defer response.Body.Close()
	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		logger.I("couldn't read body")
	}

	if response.StatusCode != http.StatusOK {
		logger.D(string(body))
		t.Fail()
	}
	logger.D(string(body))
}