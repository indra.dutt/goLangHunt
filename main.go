package main

import (
	"gitlab.com/multitech/components/go_server/webrouter"
	"github.com/gin-gonic/gin"
	"net/http"
	"gitlab.com/multitech/components/go_server/historian"
	"gitlab.com/multitech/components/go_server/config"
	"gitlab.com/multitech/components/go_server/storage"
)

// initialize components used by this module. These can
// be called multiple times. Its just here (and invoked once)
// for convenience and readability.
var indispensableDataStore = storage.InitIndispensableDataServices()
var logger = historian.InitLoggingServices()
var routingController = webrouter.InitWebRoutingServices()
var configReader = config.InitConfigurationServices()

func main() {
	// setup the web server routes
	r := routingController.GetRouter()
	r.GET("/", rootFunc)
	r.GET("/db", dbFunc)

	// do some logging
	logger.SetLevel(historian.LOG_LEVEL_D)
	logger.D("Reading startup configuration variables from environment")

	// read server configuration and handle any errors
	port, e := configReader.Var(config.PORT)
	if e != nil {
		logger.F(e)
	}

	// start the web server
	routingController.RunInsecureRouter(port)
}

func dbFunc(c *gin.Context) {
	if err := indispensableDataStore.WriteIndispensableData(storage.QUERY_CREATE_EATER_TABLE); err != nil {
		c.JSON(http.StatusInternalServerError, err)
		return
	}
	c.JSON(http.StatusOK, gin.H{"msg": "Tables created!"})
}

func rootFunc(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{"Hi": "Indra!"})
}