package oopsies

import (
	"fmt"
)

// our error code type
type OopsCode uint16

// codes
const (
	// config errors
	OOPS_MISSING_CONFIG_VAR OopsCode = iota
	OOPS_MISSING_DOT_ENV

	// database errors
	OOPS_NO_SUCH_QUERY
	OOPS_QUERY_ID_MISMATCH
	OOPS_QUERY_EXECUTION
	OOPS_DB_OPEN

	OOPS_PARSE_PAYLOAD
	OOPS_NOT_IMPLEMENTED

	// last-resort errors. KEEP AT THE END.
	OOPS_ERRDEF_MISSING
	OOPS_ERRDEF_MISMATCH
)

// messages
// Codes (with type ErrorCode) are inside WWError to enforce that no one can
// create a WWError object without an already existing ErrorCode
// Note: no periods after error message
var errDefs = []Oops {
	// config errors
	{OOPS_MISSING_CONFIG_VAR, "MISSING_CONFIG_VAR", "Configuration environment variable not set"},
	{OOPS_MISSING_DOT_ENV, "MISSING_DOT_ENV", "Configuration file '.env' not found"},

	// database errors
	{OOPS_NO_SUCH_QUERY, "NO_SUCH_QUERY", "Could not find query with given ID"},
	{OOPS_QUERY_ID_MISMATCH, "QUERY_ID_MISMATCH", "Query ID does not match one in definition"},
	{OOPS_QUERY_EXECUTION, "QUERY_EXECUTION_FAILED", "Failed to execute SQL query"},
	{OOPS_DB_OPEN, "DB_OPEN_FAILED", "Failed to open database"},

	// payload error
	{OOPS_PARSE_PAYLOAD, "PAYLOAD_ERROR", "Failed to parse payload"},

	//generic error
	{OOPS_NOT_IMPLEMENTED, "NOT_IMPLEMENTED", "Not implemented"},

	// reserved. DO NOT USE.
	{OOPS_ERRDEF_MISSING, "ERRDEF_MISSING", "Error code exists, but no associated definition found"},
	{OOPS_ERRDEF_MISMATCH, "ERRDEF_MISMATCH", "Error code does not match the one in definition"},
}

type Oops struct {
	Code  OopsCode
	Label string
	Cause string
}

func (e Oops) Error() string {
	return fmt.Sprintf("%s(%d) Cause: %s", e.Label, e.Code, e.Cause)
}

func (e *Oops) Append(moreInfo string) *Oops {
	e.Cause += ". " + moreInfo
	return e
}
