package oopsies

import (
	"sync"
	"strconv"
)

type ErrorStore interface {
	GetError(code OopsCode) *Oops
}

type ErrorManagementServices struct {

}

func (ems ErrorManagementServices) GetError(code OopsCode) *Oops {
	if int(code) >= len(errDefs) {
		// print the code for which definition is missing
		// please add definition if you see this error
		errcopy := errDefs[OOPS_ERRDEF_MISSING]
		errcopy.Append(strconv.Itoa(int(code)))
		return &errcopy
	}

	errcopy := errDefs[code]

	if errcopy.Code != code {
		errcopy = errDefs[OOPS_ERRDEF_MISMATCH]
		errcopy.Append(strconv.Itoa(int(code)))
	}

	return &errcopy
}

var errorManager ErrorManagementServices
var singletonCreator sync.Once

func InitErrorManagementServices() ErrorStore {
	singletonCreator.Do(func() {
		errorManager = ErrorManagementServices{}
	})
	return errorManager
}

