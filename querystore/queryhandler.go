package querystore

import (
	"sync"
	"gitlab.com/multitech/components/go_server/oopsies"
	"gitlab.com/multitech/components/go_server/historian"
)

var logger = historian.InitLoggingServices()

type QueryStore struct{

}

type IndispensableDataQueries interface {
	GetDataQuery(queryId SqlQueryId) (IndispensableDataQuery, *oopsies.Oops)
	GetInitializationQueries() ([]IndispensableDataQuery, *oopsies.Oops)
}

func (store QueryStore) GetDataQuery(queryId SqlQueryId) (IndispensableDataQuery, *oopsies.Oops) {
	if int(queryId) >= len(sqlQueryDefs) {
		return IndispensableDataQuery{}, errManager.GetError(oopsies.OOPS_NO_SUCH_QUERY)
	}

	if sqlQueryDefs[queryId].Id != queryId {
		return IndispensableDataQuery{}, errManager.GetError(oopsies.OOPS_NO_SUCH_QUERY)
	}

	return sqlQueryDefs[queryId], nil
}

func (store QueryStore) GetInitializationQueries() ([]IndispensableDataQuery, *oopsies.Oops){
	var queryArr []IndispensableDataQuery
	q, err := store.GetDataQuery(QUERY_CREATE_EATER_TABLE)
	if err != nil {
		logger.E(err)
		return nil, err
	}

	queryArr = append(queryArr, q)
	return queryArr, nil
}

var SingletonCreator sync.Once
var queries QueryStore

func InitIndispensableQueryStore() IndispensableDataQueries{
	SingletonCreator.Do(func() {
		queries = QueryStore{}
	})
	return queries
}