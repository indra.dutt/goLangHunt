package querystore

import "gitlab.com/multitech/components/go_server/oopsies"

var errManager = oopsies.InitErrorManagementServices()

type SqlQueryId uint

type IndispensableDataQuery struct {
	Id SqlQueryId
	QueryFormatString string
}

const (
	QUERY_CREATE_EATER_TABLE SqlQueryId = iota
	QUERY_CREATE_ESTABLISHMENT_TABLE
	QUERY_CREATE_ESTABLISHMENT_COMMUNITY_TABLE
	QUERY_ADD_EATER
)

var sqlQueryDefs = []IndispensableDataQuery {
	// user errors
	{QUERY_CREATE_EATER_TABLE, "CREATE TABLE IF NOT EXISTS public.eater (email varchar(254) NOT NULL, id varchar(43) NOT NULL, passwordhash varchar(43) NOT NULL, passwordsalt varchar(43) NOT NULL, contactdetails jsonb NOT NULL, phone jsonb NOT NULL,PRIMARY KEY (email))"},
	{QUERY_CREATE_ESTABLISHMENT_TABLE, "CREATE TABLE public.establishment (id varchar(43) NOT NULL, userid varchar(43) NOT NULL, name varchar(254) NOT NULL, communityid varchar(43) NOT NULL, address jsonb, moves boolean,PRIMARY KEY (id))"},
	{QUERY_CREATE_ESTABLISHMENT_COMMUNITY_TABLE, "CREATE TABLE public.establishmentcommunity (id varchar(43) NOT NULL, establishmentid varchar(43) NOT NULL, name varchar(254) NOT NULL, address jsonb NOT NULL, PRIMARY KEY (id))"},
	{QUERY_ADD_EATER, "INSERT INTO public.eater (email, id, passwordhash, passwordsalt, contactdetails) VALUES ('%s', '%s', '%s', '%s', '%s'::jsonb)"},
}