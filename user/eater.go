package user

import (
	"gitlab.com/multitech/components/go_server/storage"
	"encoding/json"
	"gitlab.com/multitech/components/go_server/oopsies"
	"encoding/base64"
	"math/rand"
	"crypto/sha256"
	"sync"
	"gitlab.com/multitech/components/go_server/historian"
	"gitlab.com/multitech/components/go_server/querystore"
)

var indispensableDataStore = storage.InitIndispensableDataServices()
var logger = historian.InitLoggingServices()
var errorManager = oopsies.InitErrorManagementServices()

type UserManagementServices struct {
}

type UserFactory interface {
	CreateEaterFromJson(jsonUser string) (Eater, *oopsies.Oops)
}

type UserDbClerk interface {
	AddEater(eater Eater) (bool, *oopsies.Oops)
	UpdateEater(id string, eater Eater) (bool, *oopsies.Oops)
	DeleteEater(id string) (bool, *oopsies.Oops)
	GetEater(id string) (Eater, *oopsies.Oops)
}

type Eater struct {
	Id string
	Email string
	Password string
	PasswordHash string
	PasswordSalt string
	OtherDetails
}

type OtherDetails struct {
	FirstName string `json:"firstName"`
	LastName string `json:"lastName"`
}

func (ums UserManagementServices) CreateEaterFromJson(jsonUser string) (Eater, *oopsies.Oops) {
	var eater Eater

	if err := json.Unmarshal([]byte(jsonUser), &eater); err != nil {
		oops :=  errorManager.GetError(oopsies.OOPS_PARSE_PAYLOAD)
		logger.E(oops)
		return Eater{}, oops
	}
	eater.Id = base64.URLEncoding.EncodeToString([]byte(eater.Email))
	salt, _ := generateSalt(sha256.Size)
	eater.PasswordSalt = base64.URLEncoding.EncodeToString(salt)
	eater.PasswordHash = base64.URLEncoding.EncodeToString([]byte(eater.Password + string(salt)))
	return eater, nil
}

func (ums UserManagementServices) AddEater(eater Eater) (bool, *oopsies.Oops) {
	otherDetails, _ := json.Marshal(eater.OtherDetails)
	if err := indispensableDataStore.WriteIndispensableData(querystore.QUERY_ADD_EATER,
		eater.Email, eater.Id, eater.PasswordHash, eater.PasswordSalt, string(otherDetails)); err != nil {
		logger.E(err)
		return false, err
	}
	return true, nil
}

func (ums UserManagementServices) UpdateEater(id string, eater Eater) (bool, *oopsies.Oops) {
	return true, errorManager.GetError(oopsies.OOPS_NOT_IMPLEMENTED)
}

func (ums UserManagementServices) DeleteEater(id string) (bool, *oopsies.Oops)  {
	return true, errorManager.GetError(oopsies.OOPS_NOT_IMPLEMENTED)
}

func (ums UserManagementServices) GetEater(id string) (Eater, *oopsies.Oops) {
	return Eater{}, errorManager.GetError(oopsies.OOPS_NOT_IMPLEMENTED)
}

func generateSalt(length int) ([]byte, error) {
	salt := make([]byte, length)
	_, err := rand.Read(salt)
	if err != nil {
		return nil, err
	}
	return salt, nil
}

var userManager UserManagementServices
var singleton sync.Once

func initUserServices() {
	singleton.Do(func() {
		userManager = UserManagementServices{}
	})
}

func InitUserCreationServices() UserFactory {
	initUserServices()
	return userManager
}

func InitUserDbServices() UserDbClerk {
	initUserServices()
	return userManager
}