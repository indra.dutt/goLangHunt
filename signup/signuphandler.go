package signup

import (
	"sync"
	"github.com/gin-gonic/gin"
	"gitlab.com/multitech/components/go_server/user"
	"gitlab.com/multitech/components/go_server/historian"
	"net/http"
	"gitlab.com/multitech/components/go_server/oopsies"
)

var logger = historian.InitLoggingServices()
var userFactory = user.InitUserCreationServices()
var dbClerk = user.InitUserDbServices()
var errorManager = oopsies.InitErrorManagementServices()

const (
	EATER_ROUTE string = "/eaters"
)

type HttpSignupServices struct {

}

type SignupHttpDelegate interface {
	AddEater(httpRequest *gin.Context)
	DeleteEater(httpRequest *gin.Context)
	UpdateEater(httpRequest *gin.Context)
}

func (httpReqHandler HttpSignupServices) AddEater(c *gin.Context) {
	bodyRaw, err := c.GetRawData()
	if err != nil  {
		oops := errorManager.GetError(oopsies.OOPS_PARSE_PAYLOAD).Append(" Failed to fetch raw data from body.")
		logger.E(oops)
		c.JSON(http.StatusBadRequest, oops)
		return
	}
	eaterJson, eaterError := userFactory.CreateEaterFromJson(string(bodyRaw))
	if eaterError != nil {
		logger.E(eaterError)
		c.JSON(http.StatusBadRequest, eaterError)
		return
	}
	_, errorResult := dbClerk.AddEater(eaterJson)
	if errorResult != nil {
		logger.E(errorResult)
		c.JSON(http.StatusBadRequest, errorResult)
		return
	}
	c.JSON(http.StatusOK, gin.H{"msg" : "User is registered"})
}

func (httpReqHandler HttpSignupServices) DeleteEater(c *gin.Context) {

}

func (httpReqHandler HttpSignupServices) UpdateEater(c *gin.Context) {

}

var httpDelegate HttpSignupServices
var singletonCreator sync.Once

func InitHttpServices() SignupHttpDelegate {
	singletonCreator.Do(func() {
		httpDelegate = HttpSignupServices{}
	})
	return httpDelegate
}