package storage

import (
	"fmt"
	"sync"
	"gitlab.com/multitech/components/go_server/historian"
	"gitlab.com/multitech/components/go_server/oopsies"
	"database/sql"
	_ "github.com/lib/pq"
	"gitlab.com/multitech/components/go_server/config"
	"gitlab.com/multitech/components/go_server/querystore"
)

var logger = historian.InitLoggingServices()
var errorStore = oopsies.InitErrorManagementServices()
var configurator = config.InitConfigurationServices()
var queries = querystore.InitIndispensableQueryStore()

type IndispensableDataStore interface {
	WriteIndispensableData(queryId querystore.SqlQueryId, queryParts ...string) *oopsies.Oops
	ReadIndispensableData(queryId querystore.SqlQueryId, queryParts ...string) *oopsies.Oops
}

type SqlDataStore struct {
	db *sql.DB
}

func (sds SqlDataStore) ReadIndispensableData(queryId querystore.SqlQueryId, queryParts ...string) *oopsies.Oops {
	queryString, e := makeQueryString(queryId, queryParts...)
	if e != nil {
		return e
	}
	// todo: execute query instead
	logger.D("Executing read query " + queryString)
	return nil
}

func (sds SqlDataStore) WriteIndispensableData(queryId querystore.SqlQueryId, queryParts ...string) *oopsies.Oops {
	queryString, e := makeQueryString(queryId, queryParts...)
	if e != nil {
		return e
	}
	logger.D("Executing write query " + queryString)
	if _, err := sds.db.Exec(queryString); err != nil {
		return errorStore.GetError(oopsies.OOPS_QUERY_EXECUTION).Append(err.Error())
	}
	return nil
}

func makeQueryString(queryId querystore.SqlQueryId, queryParts ...string) (string, *oopsies.Oops)  {
	query, e := queries.GetDataQuery(queryId)
	if e != nil {
		logger.E(e)
		return "", e
	}
	var queryString string
	if len(queryParts) > 0 {
		queryString = fmt.Sprintf(query.QueryFormatString, queryParts)
		queryArgs := make([]interface{}, len(queryParts))
		for i, part := range queryParts {
			queryArgs[i] = part
		}
		queryString = fmt.Sprintf(query.QueryFormatString, queryArgs...)
	} else {
		queryString = query.QueryFormatString
	}
	return queryString, nil
}


var singletonCreator sync.Once
var indispensableDataStore SqlDataStore

func InitIndispensableDataServices() IndispensableDataStore {
	singletonCreator.Do(func() {
		dbUrl, ouch := configurator.Var("DATABASE_URL")
		if ouch != nil {
			logger.F(ouch)
		}
		dbHandle, err := sql.Open("postgres", dbUrl)
		if err != nil {
			logger.F(errorStore.GetError(oopsies.OOPS_DB_OPEN).Append(err.Error()))
		}
		indispensableDataStore = SqlDataStore{db:dbHandle}
		queryArr, err := queries.GetInitializationQueries()
		if err != nil {
			logger.I(err)
		}
		for _, q := range queryArr  {
			indispensableDataStore.db.Exec(q.QueryFormatString)
		}
	})
	return indispensableDataStore
}
